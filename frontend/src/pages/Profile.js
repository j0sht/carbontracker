import React, {useEffect, useState} from "react";

import Avatar from "@material-ui/core/Avatar";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";
import List from "@material-ui/core/List";
import DriveEtaIcon from '@material-ui/icons/DriveEta';
import GroupIcon from '@material-ui/icons/Group';
import Button from "@material-ui/core/Button";
import authService from "../services/auth.service";
import { deepPurple } from '@material-ui/core/colors';
import {ImExit} from "@react-icons/all-files/im/ImExit";
import GroupAddIcon from '@material-ui/icons/GroupAdd';
// For Dropdown
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteDialog from "../components/DeleteDialog";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { getFirstCharacter } from "../utils/helpers";
import _ from "validator/es";

const useStyles = makeStyles((theme) => {
  return {
    root: {
      flexGrow: 1,
    },
    purple: {
      color: theme.palette.getContrastText(deepPurple[500]),
      backgroundColor: deepPurple[500],
      width: "175px",
      height: "175px",
    }
  };
});

const Profile = () => {
  const classes = useStyles();
  const [vehicles, setVehicles] = useState([]);
  const [vehicleMakes, setVehicleMakes] = useState([]);
  const [availVehicleYears, setAvailVehicleYears] = useState([]);
  const [availVehicleModels, setAvailVehicleModels] = useState([]);
  const [vehicleModelId, setVehicleModelId] = useState([]);
  const [make, setMake] = useState('');
  const [makeIsEmpty, setMakeIsEmpty] = useState(true);
  const [makeError, setMakeError] = useState(false);
  const [model, setModel] = useState('');
  const [modelIsEmpty, setModelIsEmpty] = useState(true);
  const [modelError, setModelError] = useState(false);
  const [year, setYear] = useState('');
  const [yearIsEmpty, setYearIsEmpty] = useState(true);
  const [yearError, setYearError] = useState(false);
  const [addVehicleDialogOpen, setAddVehicleDialogOpen] = useState(false);
  const [user] = useState(authService.getCurrentUser());
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState(null);

  const [allGroups, setAllGroups] = useState([]);
  const [allGroupsAreEmpty, setAllGroupsAreEmpty] = useState(true);
  const [userGroupInfo, setUserGroupInfo] = useState({});
  const [groupName, setGroupName] = useState('');
  const [userGroupId, setUserGroupId] = useState('');
  const [groupError, setGroupError] = useState(false);
  const [addGroupDialogOpen, setAddGroupDialogOpen] = useState(false);
  const [selectGroupDialogOpen, setSelectGroupDialogOpen] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState('');
  const [selectedGroupError, setSelectedGroupError] = useState(false);
  const [loading, setLoading] = useState(false);


  const fetchUserGroupInfo =  (uid) => {
     fetch(`/api/get/getUserGroupInfo/${user.uid}`)
        .then((res) => res.json())
        .then((data) => {
          if (data.length !== 0) {
            setUserGroupInfo(data[0]);
            setGroupName(data[0].gname);
            setUserGroupId(data[0].gid);
            console.log(data[0]);
          }
          else {
            console.log("User does not belong to a group");
          }
        });
  }
    useEffect(() => {
       fetchUserGroupInfo(user.uid);
    }, []);

    useEffect(() => {
    fetch( `/api/get/getAllGroups`)
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            setAllGroups(data);
          }
        });
  }, []);

  useEffect(() => {
    fetch( '/api/get/getUniqueVehicleMakes')
      .then((res) => res.json())
      .then((data) => {
        setVehicleMakes(data);
      });
  }, []);

  useEffect(() => {
    fetch(`/api/get/getAllVehicles/${user.uid}`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setVehicles(data);
        }
      });
  }, [user]);

  const onGroupDialogOpen = () => setAddGroupDialogOpen(true);
  const onGroupDialogClose = () => {
    setAddGroupDialogOpen(false);
    setGroupError(false);
  }

  const onSelectGroupDialogOpen = () => setSelectGroupDialogOpen(true);
  const onSelectGroupDialogClose = () => {
    setSelectGroupDialogOpen(false);
    setSelectedGroupError(false);
  }

  const onDeleteMyGroup = () => {
    setLoading(true);
    fetch(`/api/delete/deleteGroup/${user.uid}/${userGroupId}`, {
          method: 'DELETE'
      }).then((res) => {
          if (res.ok) {
              console.log('Successfully deleted the group');
          } else {
              console.log('Unable to delete. check the log');
          }
     });
    setUserGroupId('');
    setGroupName('');
    setUserGroupInfo({});
    setLoading(false);
  }

  const onDialogOpen = () => setAddVehicleDialogOpen(true);
  const onDialogClose = () => {
    setAddVehicleDialogOpen(false);
    setMake('');
    setMakeIsEmpty(true);
    setMakeError(false);
    setModel('');
    setModelIsEmpty(true);
    setModelError(false);
    setYear('');
    setYearIsEmpty(true);
    setYearError(false);
  };

  const onJoinGroup = () => {
    const selectedGroupIsEmpty = selectedGroup === '';
     if (selectedGroupIsEmpty){
       setSelectedGroupError(selectedGroupIsEmpty);
       return;
     }
     setLoading(true);

     const joinedId = selectedGroup;

    fetch(`/api/post/joinGroup/${user.uid}/${joinedId}`, {
      method: 'POST',
      headers: {'Content-type': 'application/json'},
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully joined group');
      } else {
        console.log('Error joining group');
      }
      setLoading(false);
    });

    fetch(`/api/get/getUserGroupInfo/${user.uid}`, {
    }).then((res) => res.json())
        .then((data) => {
          if (data.length !== 0) {
            setUserGroupInfo(data[0]);
            setGroupName(data[0].gname);
            setUserGroupId(data[0].gid);
            console.log(data[0]);
          }
          else {
            console.log("User does not belong to a group");
          }
        });
    setUserGroupInfo({'uid':user.uid, 'mname':user.username,'gname': groupName, 'membernum':userGroupInfo.membernum });

    onSelectGroupDialogClose();
  }

  const onAddGroup = () => {
      const groupNameIsEmpty = groupName === '';
      if (groupNameIsEmpty) {
        setGroupError(groupNameIsEmpty);
        return;
      }
      setLoading(true);

      const newGroup = {
        'gname': groupName,
        'uid': user.uid,
        'mname': user.username
     }

    fetch('/api/post/addGroup', {
      method: 'POST',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify(newGroup),
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully created group');
      } else {
        console.log('Error creating group');
      }
      setLoading(false);
    });

    fetch(`/api/get/getUserGroupInfo/${user.uid}`, {
    }).then((res) => res.json())
        .then((data) => {
          if (data.length !== 0) {
            setUserGroupInfo(data[0]);
            setGroupName(data[0].gname);
            setUserGroupId(data[0].gid);
            console.log(data[0]);
          }
          else {
            console.log("User does not belong to a group");
          }
        });

    setUserGroupInfo({'uid':user.uid, 'mname':user.username,'gname': groupName, 'membernum':1 });
    onGroupDialogClose();
  }

  const onLeaveMyGroup = () => {
    fetch('/api/post/leaveGroup/' + user.uid + "/" + userGroupId, {
      method: 'POST'
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully left the group');
      } else {
        console.log('Unable to leave the group');
      }
      setUserGroupId('');
        setGroupName('');
        setUserGroupInfo([]);
    });
  }

  const onAddVehicle = () => {
    if (makeIsEmpty || modelIsEmpty || yearIsEmpty) {
      setMakeError(makeIsEmpty);
      setModelError(modelIsEmpty);
      setYearError(yearIsEmpty);
      return;
    }

    // fetch vehicle model id
    fetch( '/api/get/getVehicleModelId/' + make + "/" + year + "/" + model)
    .then((res) => res.json())
    .then((data) => {
      setVehicleModelId(data[0].id);
    });

    const newVehicle = {
      'uid': user.uid,
      'model_id': vehicleModelId,
      'make': make,
      'model': model,
      'year': year,
    };

    fetch('/api/post/addVehicle', {
      method: 'POST',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify(newVehicle),
    }).then((res) => {
      return (res.ok) ? res.json() : Promise.resolve({error: true});
    }).then((data) => {
      if (data.error) {
        console.log('error adding vehicle');
        return;
      }
      newVehicle['vid'] = data.vid;
      setVehicles(vehicles.concat([newVehicle]));
    });

    onDialogClose();
  };

  const closeDeleteDialog = () => {
    setIdToDelete(null);
    setIsDeleteDialogOpen(false)
  };

  const onDelete = () => {
    fetch(`/api/delete/deleteVehicle/${idToDelete}`, {
      method: 'DELETE'
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully deleted');
      } else {
        console.log('Unable to delete');
      }
      fetch(`/api/get/getAllVehicles/${user.uid}`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setVehicles(data);
        }
        closeDeleteDialog();
      });
    });
  };


  return (
    <div className={classes.root}>
      <Grid container direction="column" spacing={2}>
        <Grid container justifyContent="center">
          <Grid item>
          <Avatar className={classes.purple}>
            <Typography variant="h3">{getFirstCharacter(user?.username)}</Typography>
          </Avatar>
          </Grid>
        </Grid>
        <Grid item>
        {user?.username &&
          <Typography align="center" variant="h2">
            {user.username}
          </Typography>
        }
          <Typography align="center" variant="h5">
            My Group : {groupName}
          </Typography>
        </Grid>
        <Grid item>
          <List>
            <ListItem key="header">
              <ListItemText primary="Vehicles"/>
              <ListItemSecondaryAction>
                <Button variant="contained" onClick={onDialogOpen}>Add Vehicle</Button>
              </ListItemSecondaryAction>
            </ListItem>
            {vehicles.map((vehicle) => {
              const makeCapitalized = vehicle.make.charAt(0).toUpperCase() + vehicle.make.slice(1);
              return (<ListItem key={vehicle.vid}>
                <ListItemIcon>
                  <DriveEtaIcon/>
                </ListItemIcon>
                <ListItemText primary={`${makeCapitalized} ${vehicle.model}, ${vehicle.year}`}/>
                <ListItemSecondaryAction>
                        <IconButton
                          color="secondary"
                          edge="end"
                          onClick={() => {
                            setIdToDelete(vehicle.vid);
                            setIsDeleteDialogOpen(true)
                          }}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </ListItemSecondaryAction>
              </ListItem>)
            })}
          </List>
        </Grid>
      </Grid>
      <Dialog open={addVehicleDialogOpen} onClose={onDialogClose}>
        <DialogTitle>Add Vehicle</DialogTitle>
        <DialogContent>
          <Grid style={{minWidth: 300}} container direction="column" spacing={3} alignItems="stretch">
            <Grid item>
              <FormControl variant="outlined" fullWidth error={makeError}>
                <InputLabel>Make</InputLabel>
                <Select
                  native
                  label="Make"
                  value={make}
                  onChange={e => {
                    setMakeIsEmpty(e.target.value === '');
                    setMake(e.target.value);
                    setYear('');

                    fetch( '/api/get/getAvailVehicleYears/' + e.target.value)
                      .then((res) => res.json())
                      .then((data) => {
                        setAvailVehicleYears(data);
                      });

                  }}
                  inputProps={{
                    name: 'make'
                  }}
                >
                  <option key="none" aria-label="None" value="" />
                  {vehicleMakes.map(name => (
                    <option key={name.make} value={name.make}>{name.make}</option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl variant="outlined" fullWidth error={yearError}>
                <InputLabel>Year</InputLabel>
                <Select
                  label="Year"
                  native
                  value={year}
                  onChange={e => {
                    setYearIsEmpty(e.target.value === '');
                    setYear(e.target.value);

                    fetch( '/api/get/getAvailVehicleModels/' + make + "/" + e.target.value)
                      .then((res) => res.json())
                      .then((data) => {
                        setAvailVehicleModels(data);
                      });
                  }}
                  inputProps={{
                    name: 'year'
                  }}
                >
                  <option key="none" aria-label="None" value="" />
                  {availVehicleYears.map(i => (
                    <option key={i.year} value={i.year}>{i.year}</option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <Autocomplete
                fullWidth
                id="model"
                options={availVehicleModels}
                getOptionLabel={(option) => option.model}
                renderInput={(params) => <TextField {...params} error={modelError} label="Model" variant="outlined" />}
                onChange={(evt, value) => {
                  setModelIsEmpty(value.model === '');
                  setModel(value.model)
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDialogClose}>
            Cancel
          </Button>
          <Button variant="contained" onClick={onAddVehicle}>
            Add
          </Button>
        </DialogActions>
      </Dialog>
      <DeleteDialog isOpen={isDeleteDialogOpen} onclose={closeDeleteDialog} ondelete={onDelete}></DeleteDialog>

      <Grid item>
      <List>

          <ListItem key="header">
            <ListItemText primary="My Group"/>
            <ListItemSecondaryAction>
              <Button variant="contained" onClick={onGroupDialogOpen}>Create My Group</Button>
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem key={userGroupInfo.gid}>
            <ListItemIcon>
              <GroupIcon/>
            </ListItemIcon>
            <ListItemText primary={(Object.entries(userGroupInfo).length !== 0 ) ? `Name: ${userGroupInfo.gname} - Manager: ${userGroupInfo.mname}, ${userGroupInfo.membernum} members` : 'You do not belong to a group'} />
            <ListItemSecondaryAction>
              <IconButton edge="end" onClick={onSelectGroupDialogOpen}>
                <GroupAddIcon />
              </IconButton>
              <IconButton color="secondary" edge="end" onClick={onLeaveMyGroup}>
                <ImExit />
              </IconButton>
              <IconButton color="secondary" edge="end" onClick={onDeleteMyGroup}>
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        </List>
      </Grid>

      <Dialog open={addGroupDialogOpen} onClose={onGroupDialogClose}>
        <DialogTitle>Create Group</DialogTitle>
        <DialogContent>
          <Grid container direction="column" spacing={3}>
            <Grid item>
                <TextField required fullWidth variant="outlined" margin="normal"
                    label="Group Name" InputProps={{name: 'gname'}} value={groupName}
                           onChange={e => {setGroupName(e.target.value);}} error={groupError}
                />
            </Grid>
          </Grid>
          </DialogContent>
        <DialogActions>
          <Button onClick={onGroupDialogClose}>Cancel</Button>
          <Button variant="contained" onClick={onAddGroup}>Add</Button>
        </DialogActions>
      </Dialog>

  <Dialog open={selectGroupDialogOpen} onClose={onSelectGroupDialogClose}>
    <DialogTitle>Select Group to Join</DialogTitle>
    <DialogContent>
      <Grid style={{minWidth: 300}} container direction="column" spacing={3} alignItems="stretch">
        <Grid item>
          <Autocomplete fullWidth
                        id="Group Name"
                        options={allGroups}
                        getOptionLabel={(group) => `${group.gname},  Manager: ${group.mname}, ${group.membernum} members, Id ${group.gid}`}
                        renderInput={(params) => <TextField {...params} required error={selectedGroupError} label="Group Name" variant="outlined" margin="normal" />}
                        onChange={(e, value) => {
                          if (value) { setSelectedGroup(value.gid);}}}
          />
        </Grid>
      </Grid>
    </DialogContent>
    <DialogActions>
      <Button onClick={onSelectGroupDialogClose}>
        Cancel
      </Button>
      <Button variant="contained" onClick={onJoinGroup}>
        Join
      </Button>
    </DialogActions>
  </Dialog>


    </div>
  );
};

export default Profile;
