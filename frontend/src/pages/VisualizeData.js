import React, {useState } from 'react';

import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PieChartIcon from '@material-ui/icons/PieChart';
import BarChartIcon from '@material-ui/icons/BarChart';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

import {Pie, PieFromDate} from '../components/Charts/PieCharts'

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
    >
      {value === index && (
          children
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: "auto",
  },
}));

const VisualizeData = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  
  const [format, setFormat] = useState('pie');

  const handleFormat = (event, newFormat) => {
    setFormat(newFormat);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
        <ToggleButtonGroup 
          value={format} 
          exclusive
          onChange={handleFormat} 
          aria-label="Graph type">
        <ToggleButton value="pie" aria-label="bold">
          < PieChartIcon />
        </ToggleButton>
        <ToggleButton value="bar" aria-label="italic">
          < BarChartIcon />
        </ToggleButton>
      </ToggleButtonGroup>

      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="All" />
          <Tab label="From..." />
        </Tabs>
      </AppBar>
        <TabPanel value={value} index={0}>
          {(format === 'pie') ? <Pie />: 'Render barchart component'}
        </TabPanel>
        <TabPanel value={value} index={1}>
        {(format === 'pie') ? <PieFromDate />: 'Render barchart component'}
        </TabPanel>
    </div>
  );
}

export default VisualizeData;
