import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";

const Home = () => {

    return (
        <Container color="default">
            <Typography variant="h3" align="center">Welcome to Carbon Tracker</Typography>
            <Typography variant="h3" align="center">  <br/> </Typography>

            <Typography variant="h6" align="left">- In Carbon Tracker, users are able to add a vehicle in their profile file. </Typography>
            <Typography variant="h6" align="left">- Users are also able to add vehicle and flight trips in the dashboard to calculate the carbon emissions for these trips. </Typography>
            <Typography variant="h6" align="left">- Users are also able to calculate carbon emissions for items they have ordered online (carbon emissions related to shipping) in the dashboard page.</Typography>
            <Typography variant="h6" align="left">- On top of this, users can also toggle on/off dark mode. </Typography>

        </Container>
    )
};

export default Home;
