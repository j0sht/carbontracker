import React, {useCallback, useEffect, useState} from 'react';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {makeStyles, Paper} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import OrderForm from "../components/OrderForm";
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider, KeyboardDatePicker} from "@material-ui/pickers";

/* Implemented components */
import FlightTripForm from "../components/FlightTripForm";
import VehicleTripForm from "../components/VehicleTripForm";
import AuthService from "../services/auth.service"
import DeleteDialog from '../components/DeleteDialog';
import { USER_ROLES } from '../utils/constants';
import AdminSection from '../components/AdminSection';
import EditDialog from '../components/EditDialog';
import FlightList from "../components/Lists/FlightList";
import VehicleTripList from "../components/Lists/VehicleTripList";
import OrdersList from "../components/Lists/OrdersList";
import FlightLineChart from "../components/Charts/FlightLineChart";
import FlightPieChart from "../components/Charts/FlightPieChart";
import VehicleTripLineChart from "../components/Charts/VehicleTripLineChart";
import VehicleTripPieChart from "../components/Charts/VehicleTripPieChart";
import OrderLineChart from "../components/Charts/OrderLineChart";
import OrderPieChart from "../components/Charts/OrderPieChart";
import { createCSV } from '../utils/helpers';
import {Pie} from "../components/Charts/PieCharts";
import TotalsLineChart from "../components/Charts/TotalsLineChart";
import TotalsBarChart from "../components/Charts/TotalsBarChart";
import LeaderboardList from "../components/Lists/LeaderboardList";


const useStyles = makeStyles((theme) => {
  return {
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
    },
    leaderboard: {
      height: 300,
    },
    tabBar: {
      height: 72,
    }
  };
});

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      hidden={value !== index}
      id={`${index}`}
      style={{padding: 24}}
    >
      {value === index && (
        children
      )}
    </div>
  );
}

const Dashboard = () => {
  const classes = useStyles();
  const [isAddDataDialogOpen, setIsAddDataDialogOpen] = useState(false);
  const [isExportDataDialogOpen, setIsExportDataDialogOpen] = useState(false);
  const [formTabIndex, setFormTabIndex] = useState(1);
  const [exportTabIndex, setExportTabIndex] = useState(1);

  const [listTabIndex, setListTabIndex] = useState(0);
  const [flights, setFlights] = useState([]);
  const [vehicleTrips, setVehicleTrips] = useState([]);
  const [orders, setOrders] = useState([]);
  const [user] = useState(AuthService.getCurrentUser());

  const titleText = `Welcome ${user.username}`;

  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState(null);
  const [typeToDelete, setTypeToDelete] = useState('');

  const [typeToEdit, setTypeToEdit] = useState('');
  const [isEditDialogOpen, setIsEditDialogOpen] = useState(false);
  const [idToEdit, setIdToEdit] = useState(null);

  // Default to start and end of current year
  const [startDate, setStartDate] = useState(new Date(new Date().getFullYear(), 0, 1));
  const [endDate, setEndDate] = useState(new Date(new Date().getFullYear(), 11, 31));

  const closeDeleteDialog = () => {
    setIdToDelete(null);
    setTypeToDelete('');
    setIsDeleteDialogOpen(false);
    const currStart = startDate;
    setStartDate(new Date());
    setStartDate(currStart);
  };

  const closeEditDialog = () => {
    setIdToEdit(null);
    // setTypeToEdit('');
    setIsEditDialogOpen(false)
    switch (typeToEdit) {
      case 'flightTrip':
        fetchFlights(user.uid, startDate, endDate)
          .then(() => closeDeleteDialog());
        break;
      case 'vehicleTrips':
        fetchVehicleTrips(user.uid, startDate, endDate)
          .then(() => closeDeleteDialog());
        break;
      default:
        fetchOrders(user.uid, startDate, endDate)
          .then(() => closeDeleteDialog());
    }
  };

  const onDelete = () => {
    let url;
    switch (typeToDelete) {
      case 'flight':
        url = `/api/delete/deleteFlight/${idToDelete}`;
        break;
      case 'vTrip':
        url = `/api/delete/deleteVehicleTrip/${idToDelete}`;
        break;
      default:
        url = `/api/delete/deleteOrder/${idToDelete}`;
    }
    fetch(url, {
      method: 'DELETE'
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully deleted');
      } else {
        console.log('Unable to delete');
      }
      setIdToDelete(null);
      setTypeToDelete('');
      switch (typeToDelete) {
        case 'flight':
          fetchFlights(user.uid, startDate, endDate)
            .then(() => closeDeleteDialog());
          break;
        case 'vTrip':
          fetchVehicleTrips(user.uid, startDate, endDate)
            .then(() => closeDeleteDialog());
          break;
        default:
          fetchOrders(user.uid, startDate, endDate)
            .then(() => closeDeleteDialog());
      }
    });
  };

  const fetchAllData = useCallback(
    (uid, start, end) => {
      fetchFlights(uid, start, end);
      fetchVehicleTrips(uid, start, end);
      fetchOrders(uid, start, end);
    }, []);


  useEffect(() => {
    fetchAllData(user.uid, startDate, endDate);
  }, [user.uid, startDate, endDate, fetchAllData]);

  const onAddData = () => {
    setIsAddDataDialogOpen(true);
  };

  const onExportData = () => {
    setIsExportDataDialogOpen(true)
  }

  const onDialogClose = () => {
    setIsAddDataDialogOpen(false);
    fetchAllData(user.uid, startDate, endDate);
    const currStart = startDate;
    setStartDate(new Date());
    setStartDate(currStart);
  };

  const onExportDialogClose = () => {
    setIsExportDataDialogOpen(false);
  }

  const onUpdateFormTabIndex = (_, index) => {
    setFormTabIndex(index);
  };

  const onUpdateExportTabIndex = (_, index) => {
    setExportTabIndex(index);
  };

  const onUpdateListTabIndex = (_, index) => {
    setListTabIndex(index);
  }

  const fetchFlights = async (uid, start, end) => {
    const startDateFormatted = start.toISOString().split('T')[0];
    const endDateFormatted = end.toISOString().split('T')[0];
    await fetch(`/api/get/getAllFlightTripsInDateRange/${uid}/${startDateFormatted}/${endDateFormatted}`)
      .then((res) => res.json())
      .then((data) => {
        setFlights(data);
      });
  }

  const fetchVehicleTrips = async (uid, start, end) => {
    const startDateFormatted = start.toISOString().split('T')[0];
    const endDateFormatted = end.toISOString().split('T')[0];
    await fetch(`/api/get/getAllVehicleTripsInDateRange/${uid}/${startDateFormatted}/${endDateFormatted}`)
      .then((res) => res.json())
      .then((data) => {
        setVehicleTrips(data);
      });
  }

  const fetchOrders = async (uid, start, end) => {
    const startDateFormatted = start.toISOString().split('T')[0];
    const endDateFormatted = end.toISOString().split('T')[0];
    await fetch(`/api/get/getAllOrdersInDateRange/${uid}/${startDateFormatted}/${endDateFormatted}`)
      .then((res) => res.json())
      .then((data) => {
        setOrders(data);
      });
  }

  const handleStartDateChanged = (date) => {
    if (!isNaN(date)) {
      setStartDate(date);
      fetchAllData(user.uid, date, endDate);
    }
  };

  const handleEndDateChanged = (date) => {
    if (!isNaN(date)) {
      setEndDate(date);
      fetchAllData(user.uid, startDate, date);
    }
  }

  return (
    <div className={classes.root}>
      <Grid container direction="column" spacing={2}>
      {user?.role === USER_ROLES.user ? (
        <>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item sm={8}>
            <Typography variant="h3">{titleText}</Typography>
          </Grid>
          <Grid item sm={4}>
            <Grid container spacing={2} justifyContent="flex-end">
              <Grid item>
                <Button variant="contained" onClick={onAddData}>Add Data</Button>
              </Grid>
              <Grid item>
                <Button variant="contained" onClick={onExportData}>Export Data</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container className={classes.tabBar} justifyContent="space-between" alignItems="baseline">
          <Grid item>
            <Tabs value={listTabIndex} onChange={onUpdateListTabIndex}>
              <Tab label="Summary" />
              <Tab label="Flight Trips" />
              <Tab label="Vehicle Trips" />
              <Tab label="Orders" />
            </Tabs>
          </Grid>
          {(listTabIndex === 0) ? <div style={{height: 53}}/> : <Grid item>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid container justifyContent="space-between">
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  label="Start Date"
                  value={startDate}
                  onChange={handleStartDateChanged}
                />
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  label="End Date"
                  value={endDate}
                  onChange={handleEndDateChanged}
                />
              </Grid>
            </MuiPickersUtilsProvider>
          </Grid>}
        </Grid>
        <Grid item>
          <TabPanel value={listTabIndex} index={0}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Grid container justifyContent="space-around" spacing={2}>
                  <Grid item md={4} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <LeaderboardList />
                    </Paper>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <Pie />
                    </Paper>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <TotalsBarChart />
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Paper elevation={3} className={classes.paper}>
                  <TotalsLineChart />
                </Paper>
              </Grid>
            </Grid>
          </TabPanel>
        </Grid>
        <Grid item>
          <TabPanel value={listTabIndex} index={1}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Grid container justifyContent="space-around" spacing={2}>
                  <Grid item sm={6} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <FlightLineChart startDate={startDate} endDate={endDate} />
                    </Paper>
                  </Grid>
                  <Grid item sm={6} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <FlightPieChart startDate={startDate} endDate={endDate} />
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Paper elevation={3} className={classes.paper}>
                  <FlightList
                    flights={flights}
                    setIdToDelete={setIdToDelete}
                    setTypeToDelete={setTypeToDelete}
                    setIsDeleteDialogOpen={setIsDeleteDialogOpen}
                    setIdToEdit={setIdToEdit}
                    setTypeToEdit={setTypeToEdit}
                    setIsEditDialogOpen={setIsEditDialogOpen}
                  />
                </Paper>
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={listTabIndex} index={2}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Grid container justifyContent="space-around" spacing={2}>
                  <Grid item sm={6} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <VehicleTripLineChart startDate={startDate} endDate={endDate} />
                    </Paper>
                  </Grid>
                  <Grid item sm={6} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <VehicleTripPieChart startDate={startDate} endDate={endDate} />
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Paper elevation={3} className={classes.paper}>
                  <VehicleTripList
                    vehicleTrips={vehicleTrips}
                    setIdToDelete={setIdToDelete}
                    setTypeToDelete={setTypeToDelete}
                    setIsDeleteDialogOpen={setIsDeleteDialogOpen}
                    setIdToEdit={setIdToEdit}
                    setTypeToEdit={setTypeToEdit}
                    setIsEditDialogOpen={setIsEditDialogOpen}
                  />
                </Paper>
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={listTabIndex} index={3}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Grid container justifyContent="space-around" spacing={2}>
                  <Grid item sm={6} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <OrderLineChart startDate={startDate} endDate={endDate} />
                    </Paper>
                  </Grid>
                  <Grid item sm={6} xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                      <OrderPieChart startDate={startDate} endDate={endDate} />
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Paper elevation={3} className={classes.paper}>
                  <OrdersList
                    orders={orders}
                    setIdToDelete={setIdToDelete}
                    setTypeToDelete={setTypeToDelete}
                    setIsDeleteDialogOpen={setIsDeleteDialogOpen}
                    setIdToEdit={setIdToEdit}
                    setTypeToEdit={setTypeToEdit}
                    setIsEditDialogOpen={setIsEditDialogOpen}
                  />
                </Paper>
              </Grid>
            </Grid>
          </TabPanel>
        </Grid>
        </>) :
        <AdminSection currentUser={user}/> }
      </Grid>
      <Dialog open={isAddDataDialogOpen} onClose={onDialogClose}>
        <DialogTitle>Add Data</DialogTitle>
        <DialogContent>
          <Tabs value={formTabIndex} onChange={onUpdateFormTabIndex}>
            <Tab label="Flight Trip" />
            <Tab label="Vehicle Trip" />
            <Tab label="Order" />
          </Tabs>
          <TabPanel value={formTabIndex} index={0}>
            <FlightTripForm uid={user.uid} handleClose={onDialogClose} />
          </TabPanel>
          <TabPanel value={formTabIndex} index={1}>
            <VehicleTripForm uid={user.uid} handleClose={onDialogClose} />
          </TabPanel>
          <TabPanel value={formTabIndex} index={2}>
            <OrderForm uid={user.uid} handleClose={onDialogClose} />
          </TabPanel>
        </DialogContent>
      </Dialog>
      <Dialog open={isExportDataDialogOpen} onClose={onExportDialogClose}>
        <DialogTitle>Export Data</DialogTitle>
        <DialogContent>
          <Tabs value={exportTabIndex} onChange={onUpdateExportTabIndex}>
            <Tab label="Flight Trip" />
            <Tab label="Vehicle Trip" />
            <Tab label="Order" />
          </Tabs>
          <TabPanel value={exportTabIndex} index={0}>
            <Grid container justifyContent="center">
              <Grid item>
                <Button onClick={() => createCSV(flights, 'flights.csv')} variant="contained">Export To CSV</Button>
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={exportTabIndex} index={1}>
            <Grid container justifyContent="center">
              <Grid item>
                <Button onClick={() => createCSV(vehicleTrips, 'vehicle_trips.csv')} variant="contained">Export To CSV</Button>
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={exportTabIndex} index={2}>
            <Grid container justifyContent="center">
              <Grid item>
                <Button onClick={() => createCSV(orders, 'orders.csv')} variant="contained">Export To CSV</Button>
              </Grid>
            </Grid>
          </TabPanel>
        </DialogContent>
      </Dialog>
      <DeleteDialog isOpen={isDeleteDialogOpen} onclose={closeDeleteDialog} ondelete={onDelete} />
      <EditDialog isOpen={isEditDialogOpen} onclose={closeEditDialog} id={idToEdit} editType={typeToEdit} />
    </div>
  );
};

export default Dashboard;
