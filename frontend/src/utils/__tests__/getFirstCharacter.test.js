import { getFirstCharacter } from "../helpers";

describe('getFirstCharacter', () => {
    it('should return capital first letter for string', () => {
        
        const first = getFirstCharacter('test');
        expect(first).toEqual('T');
    })
    it('should return number for string starting with number', () => {
        const first = getFirstCharacter('123test');
        expect(first).toEqual('1');
    })
});

