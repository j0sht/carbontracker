import moment from 'moment'

// return uppercase first character of given string, if not a string just return first character
export const getFirstCharacter = (str) => {
    return typeof str === 'string' ? str.charAt(0).toUpperCase() : str.charAt(0);
}

export const getToday = () => {
    const t = new Date();
    return moment(t).format("YYYY-MM-DD")
}

export const carbonInfo = (carbonG, carbonLb, carbonKg, carbonMt) => {
    return `Carbon Amount: ${carbonG}g | ${carbonLb}lb | ${carbonKg}kg | ${carbonMt}mt`
  }

export const createCSV = (data, filename) => {
    if (!data.length) {
      return;
    }
    const array = typeof data !== 'object' ? JSON.parse(data) : data;
    // make the header
    let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}\r\n`;
    // append the rest of the data
    const csv = array.reduce((str, next) => {
        str += `${Object.values(next).map(value => `"${value}"`).join(",")}\r\n`;
        return str;
       }, str);
    /* create, click and remove the link from the DOM. This seems pretty hacky but its common practice for 
       exporting data into files on the client side */
    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    var link = document.createElement("a");
    var url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", filename);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }