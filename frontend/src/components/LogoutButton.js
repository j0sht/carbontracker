import React from "react";
import Button from "@material-ui/core/Button";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const LogoutButton = (props) => {
  const {onclick} = props;
  return (
    <Button
      color="inherit"
      onClick={onclick}
      endIcon={<ExitToAppIcon />}
    >
      Log Out
    </Button>
  );
};

export default LogoutButton;
