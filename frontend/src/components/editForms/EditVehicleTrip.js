import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";

import { Loading } from '../index'
import { getToday } from '../../utils/helpers';


const EditVehicleTrip = ({uid, vtid, onClose}) => {
    const [allVehicles, setAllVehicles] = useState([]);
    const [selectedVehicleId, setSelectedVehicleId] = useState('');
    const [selectedVehicleError, setSelectedVehicleError] = useState(false);

    const [distance, setDistance] = useState('');
    const [dateAdded, setDateAdded] = useState(getToday());
    const [dateError, setDateError] = useState(false);
    const [distanceError, setDistanceError] = useState(false);

    const [distanceUnit, setDistanceUnit] = useState('km');

    const [loading, setLoading] = useState(false);

    // gets all vehicles owned by the user
    useEffect(() => {
        fetch(`/api/get/getAllVehicles/${uid}`)
        .then((res) => res.json())
        .then((data) => {
            setAllVehicles(data);
        });
    }, [uid]);

    const onEditVehicleTrip = () => {
        const selectedVehicleNull = selectedVehicleId == null;
        const num = parseInt(distance);
        const distanceNaN = isNaN(num);
        const invalidDate = dateAdded === '';
        if (selectedVehicleNull || distanceNaN || invalidDate) {
            setSelectedVehicleError(selectedVehicleNull);
            setDistanceError(distanceNaN);
            setDateError(invalidDate);
            return;
        }
        setLoading(true);

        const vehicle = allVehicles.find((v) => v.vid === parseInt(selectedVehicleId));
        const make = vehicle.make;
        const year = vehicle.year;
        const model = vehicle.model;

        // fetch vehicle model id
        fetch( '/api/get/getVehicleModelId/' + make + "/" + year + "/" + model)
        .then((res) => res.json())
        .then((data) => {
            return Promise.resolve(data[0].id);
        })
        .then((modelId) => {
            const newVehicleTrip = {
            'vtid': vtid,
            'vid': parseInt(selectedVehicleId),
            'uid': uid,
            'distance': num,
            'distanceunit': distanceUnit,
            'model_id': modelId,
            'date_added': dateAdded
            };
            fetch('/api/update/updateVehicleTrip', {
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                body: JSON.stringify(newVehicleTrip),
            })
            .then((res) => {
                if (res.ok) {
                    console.log('Successfully editted vehicle trip');
                    onClose();
                } else {
                    console.log('Error editting vehicle trip');
                    onClose();
                }
            })
            setLoading(false);
        });
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item>
                <FormControl fullWidth error={selectedVehicleError} variant="outlined">
                <InputLabel>Vehicle</InputLabel>
                    <Select
                        native
                        label="Vehicle"
                        value={selectedVehicleId ? selectedVehicleId : ''}
                        onChange={(e) => setSelectedVehicleId(e.target.value)}
                        required
                    >
                        <option aria-label="None" value="" />
                        {allVehicles.map((vehicle) => {
                        const fullName = `${vehicle.make} ${vehicle.model}, ${vehicle.year}`;
                        return <option key={vehicle.vid} value={vehicle.vid}>{fullName}</option>;
                        })}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item>
                <TextField
                required
                fullWidth
                variant="outlined"
                margin="normal"
                label="Distance"
                InputProps={{name: 'distance'}}
                value={distance}
                onChange={e => {
                    setDistance(e.target.value);
                }}
                error={distanceError}
                />
            </Grid>
            <Grid item>
                <FormControl component="fieldset">
                <FormLabel component="legend">Distance Unit</FormLabel>
                <RadioGroup
                    row
                    name="distanceUnit"
                    value={distanceUnit}
                    onChange={(e) => setDistanceUnit(e.target.value)}
                >
                    <FormControlLabel
                    control={<Radio color="secondary" />}
                    label="Kilometers"
                    value="km"
                    labelPlacement="start"
                    />
                    <FormControlLabel
                    control={<Radio color="secondary" />}
                    label="Miles"
                    value="mi"
                    labelPlacement="start"
                    />
                </RadioGroup>
                </FormControl>
            </Grid>
            <Grid item>
                <TextField
                id="date"
                label="Trip date"
                type="date"
                defaultValue={getToday()}
                onChange={e => {
                    setDateAdded(e.target.value);
                }}
                InputLabelProps={{
                    shrink: true,
                }}
                required
                error={dateError}
                />
            </Grid>
            <div style={{ padding: 20 }}>
                <Grid item>
                    <Grid container spacing={2} justifyContent="flex-end">
                        <Grid item>
                            <Button variant="text" onClick={onClose}>Cancel</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" onClick={onEditVehicleTrip}>Edit</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
            <Loading isLoading={loading}/>
        </Grid>
    );
}

export default EditVehicleTrip; 