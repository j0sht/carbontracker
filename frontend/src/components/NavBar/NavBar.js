import React from "react";

import MainNav from "../MainNav";
import AuthNav from "../AuthNav";

import './styles.css'

// the wrapper component for navigation, again lots needed to be done here

const NavBar = () => {
  return (
    <nav className="navbar">
        <MainNav />
        <br/>
        <AuthNav />
    </nav>
  );
};

export default NavBar;
