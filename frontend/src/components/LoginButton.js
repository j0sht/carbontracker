import React from "react";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

const LoginButton = () => {
  const history = useHistory();
  return (
    <Button
      color="inherit"
      onClick={() => history.push('/signin')}
    >
      Log In
    </Button>
  );
};

export default LoginButton;
