import React from 'react';
import { shallow } from 'enzyme';

import LogoutButton from '../../LogoutButton';

describe('Logout Button Component', () => {
    it('should render correctly and call function onclick', () => {
        const clickFn = jest.fn();
        const component = shallow(<LogoutButton onclick={clickFn} />);
        expect(component).toMatchSnapshot();
        component.simulate('click');
        expect(clickFn).toHaveBeenCalled();
    });
})