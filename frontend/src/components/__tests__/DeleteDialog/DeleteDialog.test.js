import React from 'react';
import { mount } from 'enzyme';

import DeleteDialog from '../../DeleteDialog';

describe('Delete Dialog Component', () => {
    it('should render correctly and call onclose function when closed', () => {
        const closeFn = jest.fn();
        const deleteFn = jest.fn();
        const component = mount(<DeleteDialog isOpen={true} onclose={closeFn} ondelete={deleteFn} />);
        expect(component).toMatchSnapshot();
        const buttons = component.find('button');
        expect(buttons.length).toEqual(2);
        buttons.first().simulate('click');
        expect(closeFn).toHaveBeenCalled();
        buttons.at(1).simulate('click');
        expect(deleteFn).toHaveBeenCalled();
        component.unmount();
    });
});