import React from 'react'
import LoginButton from '../../LoginButton';
import { shallow } from 'enzyme'

describe('Login Button Component', () => {
    it('should render properly', () => {
        const component = shallow(<LoginButton />);
        expect(component).toMatchSnapshot();
    })
})