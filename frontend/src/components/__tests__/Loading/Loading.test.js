import React from 'react';
import { shallow } from 'enzyme';

import Loading from '../../Loading';

describe('Loading Component', () => {
    it('should render correctly when loading is false', () => {
        const component = shallow(<Loading isLoading={false} />);
      
        expect(component).toMatchSnapshot();
    });
    it('should render correctly when loading is true', () => {
        const component = shallow(<Loading isLoading={true} />);
      
        expect(component).toMatchSnapshot();
    });
})