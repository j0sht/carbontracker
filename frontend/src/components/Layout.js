import React, {useEffect} from 'react';
import {makeStyles, useMediaQuery, useTheme} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import {useState} from "react";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import {AccountCircle, ChevronLeft, Home, SpeedRounded} from "@material-ui/icons";
import {useHistory, useLocation} from "react-router-dom";
import AuthService from '../services/auth.service';
import LoginButton from './LoginButton';
import SignupButton from './SignUpButton';
import LogoutButton from './LogoutButton';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => {
  return {
    root: {
      display: "flex",
    },
    appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    active: {
      background: '#f4f4f4'
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    page: {
      width: "100%",
      padding: theme.spacing(2),
    },
    toolbar: theme.mixins.toolbar,
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: 'space-around',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    title: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
  };
})

const Layout = ({children, toggleDarkTheme, themeIcon}) => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [currentUser, setCurrentUser] = useState(undefined);
  const [isOpenDrawer, setIsOpenDrawer] = useState(true);
  const [appBarTitle, setAppBarTitle] = useState('');
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
      history.push('/dashboard');
    }
  }, [history]);

  useEffect(() => {
    switch (location.pathname) {
      case '/dashboard':
        setAppBarTitle('Dashboard');
        break;
      case '/profile':
        setAppBarTitle('Profile');
        break;
      default:
        setAppBarTitle('Home');
    }
    setIsOpenDrawer(!isSmallScreen);
  }, [isSmallScreen, location.pathname]);

  const logout = () => {
      AuthService.logout();
      setCurrentUser(undefined);
      history.push('/')
  };

  const handleDrawerOpen = () => {
    setIsOpenDrawer(true);
  };

  const handleDrawerClose = () => {
    setIsOpenDrawer(false);
  };

  const loggedOutMenuItems = [
    {
      title: 'Home',
      icon: <Home />,
      path: '/',
    },
  ];

  const loggedInMenuItems = [
    {
      title: 'Dashboard',
      icon: <SpeedRounded />,
      path: '/dashboard',
    },
    {
      title: 'Profile',
      icon: <AccountCircle />,
      path: '/profile',
    },
  ];

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        color="default"
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: isOpenDrawer,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, isOpenDrawer && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {appBarTitle}
          </Typography>
          <IconButton
            onClick={toggleDarkTheme}
          >
            {themeIcon}
          </IconButton>
          {(currentUser ?
            <LogoutButton onclick={logout}/> :
            <>
              <LoginButton/>
              <SignupButton/>
            </>)
          }
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={isOpenDrawer}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <Typography variant="h6">
            Carbon Tracker
          </Typography>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeft />
          </IconButton>
        </div>
        <Divider />
        <List>
          {(currentUser ? loggedInMenuItems : loggedOutMenuItems).map((item) => (
            <ListItem
              button
              key={item.path}
              onClick={() => {
                history.push(item.path);
                setAppBarTitle(item.title);
              }}
            >
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.title}/>
            </ListItem>
          ))}
        </List>
      </Drawer>
      <div className={clsx(classes.content, {
        [classes.contentShift]: isOpenDrawer,
      })}>
        <div className={classes.drawerHeader} />
        {children}
      </div>
    </div>
  );
};

export default Layout;
