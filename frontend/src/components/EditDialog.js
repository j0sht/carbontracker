import React, {useState} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";

import AuthService from '../services/auth.service'
import EditFlightTrip from './editForms/EditFlightTrip'
import EditVehicleTrip from './editForms/EditVehicleTrip';
import EditOrders from './editForms/EditOrders';

const EditDialog = ({isOpen, onclose, id, editType}) => {
    const [user] = useState(AuthService.getCurrentUser());

    const returnEditForm = () => {
        switch(editType) {
            case "flightTrip": return <EditFlightTrip uid={user.uid} fid={id} onClose={onclose}/>;
            case "vehicleTrips": return <EditVehicleTrip uid={user.uid} vtid={id} onClose={onclose}/>;
            case "orders": return <EditOrders uid={user.uid} oid={id} onClose={onclose}/>;
            default: break;
        }
    }

    return (
        <Dialog open={isOpen} onClose={onclose}>
        <DialogTitle>Edit trip</DialogTitle>
        <DialogContent>
            {returnEditForm()}
        </DialogContent>
    </Dialog>
    );
};

export default EditDialog;