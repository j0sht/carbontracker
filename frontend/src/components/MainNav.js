import {NavLink} from "react-router-dom";
import React from "react";

// Note DS - just placeholder for now, this will need to be reworked to reflect what we actually want

const MainNav = () => (
  <div className="navbar-nav mr-auto">
    <NavLink
      to="/"
      exact
      className="nav-link"
      activeClassName="router-link-exact-active"
    >
      Home
    </NavLink>
    <span> </span>
    <NavLink
      to="/profile"
      exact
      className="nav-link"
      activeClassName="router-link-exact-active"
    >
      Profile
    </NavLink>
  </div>
);

export default MainNav;