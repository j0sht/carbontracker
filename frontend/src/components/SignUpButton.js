import React from "react";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

const SignupButton = () => {
  const history = useHistory();

  return (
    <Button
      color="inherit"
      onClick={() => 
        {
          history.push('/signup')
        }
      }
    >
      Sign Up
    </Button>
  );
};

export default SignupButton;
