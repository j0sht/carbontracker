import React from "react";
import styled from "styled-components"
// this is from auth0 docs, could create our own if we want
const loadingImg =
  "https://cdn.auth0.com/blog/auth0-react-sample/assets/loading.svg";

const StyledDiv = styled.div`
  position: absolute;
  justify-content: center;
  height: 100%;
  width: 100%;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  backdrop-filter: blur(3px);

  display: ${props => props.isLoading ? 'flex' : 'none'};
  z-index: ${props => props.isLoading ? 10 : 0};

`;

const Loading = ({isLoading}) => (
  <StyledDiv isLoading={isLoading}>
    <img src={loadingImg} alt="Loading..." />
  </StyledDiv>
);

export default Loading;