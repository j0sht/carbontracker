import React from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import LocalShippingIcon from "@material-ui/icons/LocalShipping";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Divider from "@material-ui/core/Divider";
import { carbonInfo } from '../../utils/helpers';

const OrdersList = ({orders, setIdToDelete, setTypeToDelete, setIsDeleteDialogOpen, setIdToEdit, setTypeToEdit, setIsEditDialogOpen}) => {
  return (
    <div>
      <List>
        {orders.map((order, i) => {
          const id_ord = order.oid;
          const weight = order.weight;
          const weightUnit = order.weight_unit;
          const dist = order.distance;
          const distUnit = order.distance_unit;
          const method = order.transmthd;
          const carbonG = order.carbon_g ? order.carbon_g : '-';
          const carbonLb = order.carbon_lb ? order.carbon_lb : '-';
          const carbonKg = order.carbon_kg ? order.carbon_kg : '-';
          const carbonMt = order.carbon_mt ? order.carbon_mt : '-';
          return (
            <div key={id_ord}>
              <ListItem key={id_ord} alignItems="flex-start">
                <ListItemIcon>
                  <LocalShippingIcon />
                </ListItemIcon>
                <ListItemText
                  primary={`Total Weight: ${weight}${weightUnit}, Total Distance: ${dist}${distUnit}, by ${method} on ${order.date_added.split('T')[0]}`}
                  secondary={carbonInfo(carbonG, carbonLb, carbonKg, carbonMt)}
                />
                <ListItemSecondaryAction>
                  <IconButton
                    color="secondary"
                    edge="end"
                    onClick={() => {
                      setIdToDelete(id_ord);
                      setTypeToDelete('order');
                      setIsDeleteDialogOpen(true)
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <IconButton
                    color="secondary"
                    edge="end"
                    onClick={() => {
                      setIdToEdit(id_ord)
                      setTypeToEdit('orders');
                      setIsEditDialogOpen(true)
                    }}
                  >
                    <EditIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider variant="fullWidth" component="li" />
            </div>
          );
        })}
      </List>
    </div>
  );
};

export default OrdersList;
