import React from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import FlightTakeoffIcon from "@material-ui/icons/FlightTakeoff";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Divider from "@material-ui/core/Divider";
import { carbonInfo } from '../../utils/helpers';

const FlightList = ({flights, setIdToDelete, setTypeToDelete, setIsDeleteDialogOpen, setIdToEdit, setTypeToEdit, setIsEditDialogOpen}) => {
  return (
    <div>
      <List>
        {/* using i here to increment an id number instead of the random id thats in the database */}
        {flights.map((flight, i) => {
          const id_fid = flight.fid;
          const carbonG = flight.carbon_g ? flight.carbon_g : '-';
          const carbonLb = flight.carbon_lb ? flight.carbon_lb : '-';
          const carbonKg = flight.carbon_kg ? flight.carbon_kg : '-';
          const carbonMt = flight.carbon_mt ? flight.carbon_mt : '-';
          return (
            <div key={id_fid}>
              <ListItem key={id_fid} alignItems="flex-start">
                <ListItemIcon>
                  <FlightTakeoffIcon />
                </ListItemIcon>

                <ListItemText
                  primary={`Passengers: ${flight.passengers}, ${flight.strt} → ${flight.dst} on ${flight.date_added.split('T')[0]}`}
                  secondary={carbonInfo(carbonG, carbonLb, carbonKg, carbonMt)}
                />
                <ListItemSecondaryAction>
                  <IconButton
                    color="secondary"
                    edge="end"
                    onClick={() => {
                      setIdToDelete(id_fid);
                      setTypeToDelete('flight');
                      setIsDeleteDialogOpen(true)
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <IconButton
                    color="secondary"
                    edge="end"
                    onClick={() => {
                      setIdToEdit(id_fid)
                      setTypeToEdit('flightTrip');
                      setIsEditDialogOpen(true)
                    }}
                  >
                    <EditIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider variant="fullWidth" component="li" />
            </div>
          )
        })}
      </List>
    </div>
  );
};

export default FlightList;
