import React, {useEffect, useState} from 'react';
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";
import {ListItem, ListItemIcon, ListItemText, makeStyles} from "@material-ui/core";
import StarsIcon from '@material-ui/icons/Stars';
import AuthService from "../../services/auth.service";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles((theme) => {
  return {
    leaderboard: {
      height: 350,
    },
  };
});

const Leaders = ({leaders}) => {
  return (
    leaders.map((leader, index) => {
        let iconColor;
        switch (index) {
          case 0:
            iconColor = '#ffd700';
            break;
          case 1:
            iconColor = '#c0c0c0';
            break;
          default:
            iconColor = '#cd7f32';
        }
        return (
          <div key={index}>
            <ListItem>
              <ListItemIcon style={{color: iconColor}}><StarsIcon /></ListItemIcon>
              <ListItemText
                primary={leader.username}
                secondary={`Total carbon: ${leader.total} KG`}
              />
            </ListItem>
            <Divider />
          </div>
        )
      })
  );
}

const LeaderboardList = () => {
  const [user] = useState(AuthService.getCurrentUser());
  const classes = useStyles();
  const startDate = new Date(new Date().getFullYear(), 0, 1);
  const endDate = new Date(new Date().getFullYear(), 11, 31);

  const [groupName, setGroupName] = useState(null);
  const [leaderboard, setLeaderboard] = useState([]);

  const fetchGroupInfo = async () => {
    const res = await fetch(`/api/get/getUserGroupInfo/${user.uid}`);
    const data = await res.json();
    return (data[0]) ? {
      'groupId': data[0].gid,
      'groupName': data[0].gname,
    } : null;
  };

  const fetchLeaders = async (gId) => {
    const startDateFormatted = startDate.toISOString().split('T')[0];
    const endDateFormatted = endDate.toISOString().split('T')[0];
    const res = await fetch(`/api/get/getThreeLeastCarbonEmitters/${user.uid}/${gId}/${startDateFormatted}/${endDateFormatted}`);
    const data = await res.json();
    return data.rows;
  };

  const fetchData = async () => {
    try {
      const groupInfo = await fetchGroupInfo();
      if (groupInfo) {
        setGroupName(groupInfo.groupName);
        const result = await fetchLeaders(groupInfo.groupId);
        const leaders = result.map((data) => {
          return {
            'username': data.username,
            'total': data.carbon_kg,
          };
        });
        setLeaderboard(leaders);
      }
    } catch (err) {
      console.log(`Error: ${err}`);
    }
  }

  useEffect(() => {
    console.log('Fetching leaderboard data');
    fetchData();
  }, []);

  return (
    <Box className={classes.leaderboard}>
      <List>
        <ListItem key="header">
          <ListItemText>
            <Typography variant="h5" align="center">Leaderboard</Typography>
          </ListItemText>
        </ListItem>
        {(leaderboard.length === 0)
          ? <ListItem key="no-data-header">
              <ListItemText>
                <Typography variant="h6" align="center">Not in a group 😔</Typography>
              </ListItemText>
            </ListItem>
          : <div>
            <ListItem key="group-name">
              <ListItemText>
                <Typography variant="subtitle1" align="center">{`Group: ${groupName}`}</Typography>
              </ListItemText>
            </ListItem>
            <Leaders leaders={leaderboard} />
          </div>}
      </List>
    </Box>
  );
};

export default LeaderboardList;
