import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContentText from "@material-ui/core/DialogContentText";

const DeleteDialog = ({isOpen, onclose, ondelete}) => {

  return (
    <Dialog open={isOpen} onClose={onclose}>
    <DialogTitle>Confirm Delete</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Are you sure you want to delete?
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button color="default" onClick={onclose}>Cancel</Button>
      <Button color="default" onClick={ondelete}>Confirm</Button>
    </DialogActions>
  </Dialog>
  );
};

export default DeleteDialog;