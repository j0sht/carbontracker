import React, {useEffect, useState} from 'react';
import {Chart, LineSeries, Title, ArgumentAxis, ValueAxis, Legend} from '@devexpress/dx-react-chart-material-ui';
import AuthService from "../../services/auth.service";
import {makeStyles} from "@material-ui/core/styles";
import { withStyles } from '@material-ui/core/styles';

// NOTE: Much of the code below was adapted from:
// https://devexpress.github.io/devextreme-reactive/react/chart/demos/line/line/

const legendStyles = () => ({
  root: {
    display: 'flex',
    margin: 'auto',
    flexDirection: 'row',
  },
});
const legendLabelStyles = theme => ({
  label: {
    paddingTop: theme.spacing(1),
    whiteSpace: 'nowrap',
  },
});
const legendItemStyles = () => ({
  item: {
    flexDirection: 'column',
  },
});

const legendRootBase = ({ classes, ...restProps }) => (
  <Legend.Root {...restProps} className={classes.root} />
);
const legendLabelBase = ({ classes, ...restProps }) => (
  <Legend.Label className={classes.label} {...restProps} />
);
const legendItemBase = ({ classes, ...restProps }) => (
  <Legend.Item className={classes.item} {...restProps} />
);
const Root = withStyles(legendStyles, { name: 'LegendRoot' })(legendRootBase);
const Label = withStyles(legendLabelStyles, { name: 'LegendLabel' })(legendLabelBase);
const Item = withStyles(legendItemStyles, { name: 'LegendItem' })(legendItemBase);

const useStyles = makeStyles((theme) => {
  return {
    chart: {
      height: 500,
      padding: theme.spacing(2),
    },
  };
});

const format = () => tick => tick;

const TotalsLineChart = () => {
  const [user] = useState(AuthService.getCurrentUser());
  const [totalCarbonEmissions, setTotalCarbonEmissions] = useState([]);
  const classes = useStyles();
  const startDate = new Date(new Date().getFullYear(), 0, 1);
  const endDate = new Date(new Date().getFullYear(), 11, 31);

  const fetchFlightMonthlyTotals = async () => {
    const startDateFormatted = startDate.toISOString().split('T')[0];
    const endDateFormatted = endDate.toISOString().split('T')[0];
    const res = await fetch(`/api/get/getMonthlyCarbonOfFlights/${user.uid}/${startDateFormatted}/${endDateFormatted}`);
    const data = await res.json();
    return data.map((row) => {
      const date = new Date(row.month);
      date.setDate(1);
      const next= date.getMonth()+1;
      date.setMonth(next); // For PST??
      return {
        'month': date.getMonth(),
        'sum': row.monthly_carbon_g,
      };
    });
  };

  const fetchVehicleTripMonthlyTotals = async () => {
    const startDateFormatted = startDate.toISOString().split('T')[0];
    const endDateFormatted = endDate.toISOString().split('T')[0];
    const res = await fetch(`/api/get/getMonthlyCarbonOfVehiclesTrips/${user.uid}/${startDateFormatted}/${endDateFormatted}`);
    const data = await res.json();
    return data.map((row) => {
      const date = new Date(row.month);
      date.setDate(1);
      const next= date.getMonth()+1;
      date.setMonth(next); // For PST??
      return {
        'month': date.getMonth(),
        'sum': row.monthly_carbon_g,
      };
    });
  };

  const fetchOrderMonthlyTotals = async () => {
    const startDateFormatted = startDate.toISOString().split('T')[0];
    const endDateFormatted = endDate.toISOString().split('T')[0];
    const res = await fetch(`/api/get/getMonthlyCarbonOfOrders/${user.uid}/${startDateFormatted}/${endDateFormatted}`);
    const data = await res.json();
    return data.map((row) => {
      const date = new Date(row.month);
      date.setDate(1);
      const next= date.getMonth()+1;
      date.setMonth(next); // For PST??
      return {
        'month': date.getMonth(),
        'sum': row.monthly_carbon_g,
      };
    });
  };

  const fetchAllTotals = async () => {
    try {
      const flightTotals = await fetchFlightMonthlyTotals();
      const vehicleTripTotals = await fetchVehicleTripMonthlyTotals();
      const orderTotals = await fetchOrderMonthlyTotals();
      const result = [];
      for (let i = 0; i < 12; i++) {
        const date = new Date((new Date().getFullYear()), i, 1);
        const month = date.toLocaleString('default', { month: 'short' });
        result.push({'month': month, 'flight': 0, 'vehicle': 0, 'order': 0});
      }
      for (let i = 0; i < flightTotals.length; i++) {
        const month = flightTotals[i].month;
        result[month].flight = flightTotals[i].sum;
      }
      for (let i = 0; i < vehicleTripTotals.length; i++) {
        const month = vehicleTripTotals[i].month;
        result[month].vehicle = vehicleTripTotals[i].sum;
      }
      for (let i = 0; i < orderTotals.length; i++) {
        const month = orderTotals[i].month;
        result[month].order = orderTotals[i].sum;
      }
      return result;
    } catch (err) {
      console.log(`Error: ${err}`);
    }
  };

  useEffect(() => {
    fetchAllTotals()
      .then((result) => setTotalCarbonEmissions(result));
  }, []);

  return (
    <Chart className={classes.chart} data={totalCarbonEmissions}>
      <ArgumentAxis tickFormat={format} />
      <ValueAxis />
      <LineSeries
        name="Flights"
        valueField="flight"
        argumentField="month"
      />
      <LineSeries
        name="Vehicle Trips"
        valueField="vehicle"
        argumentField="month"
      />
      <LineSeries
        name="Orders"
        valueField="order"
        argumentField="month"
      />
      <Title text="Monthly Trends (in Grams)"/>
      <Legend position="bottom" rootComponent={Root} itemComponent={Item} labelComponent={Label} />
    </Chart>
  );
};

export default TotalsLineChart;
