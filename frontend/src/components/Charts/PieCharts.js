import React, { useEffect, useState } from 'react';
import {
  Chart,
  PieSeries,
  Title,
  Legend,
  Tooltip,
} from '@devexpress/dx-react-chart-material-ui';
import { EventTracker } from '@devexpress/dx-react-chart';
import { Animation } from '@devexpress/dx-react-chart';

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

import AuthService from "../../services/auth.service";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => {
  return {
    chart: {
      maxHeight: 350,
      padding: theme.spacing(2),
    },
  };
});

export const Pie = () => {
    const classes = useStyles();
    const [user] = useState(AuthService.getCurrentUser());
    const [totalCarbonEmissions, setTotalCarbonEmissions] = useState([]);

    useEffect(() => {
        let isMounted = true;
        fetch(`/api/get/getTotalCarbon/${user.uid}`)
            .then((res) => res.json())
            .then((data) => {
              if (isMounted) {
                setTotalCarbonEmissions(data);
              }
            });
            return () => { isMounted = false};
    }, [user.uid]);

    return (
      <div>
        <Chart
            className={classes.chart}
            data={totalCarbonEmissions}
            >
            <PieSeries
            valueField="value"
            argumentField="label"
            />
            <Title
            text="Total Carbon Emissions (in Grams)"
            />
            <Legend
            orientation="vertical"
            itemTextPosition="right"
            horizontalAlignment="center"
            verticalAlignment="bottom"
            columnCount={4}
            />
            <EventTracker />
            <Tooltip />
            <Animation />
        </Chart>
      </div>
    );
  }

  export const PieFromDate = () => {
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [showPie, setShowPie] = useState(false);

    function buttonAction() {
      setShowPie(true)
    }
    return (
      <div>
        <Grid container direction="row" spacing={2}>
          <Grid item>
            <TextField
              id="date"
              label="From"
              type="date"
              onChange={e => {
                setStartDate(e.target.value);
                }}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>

          <Grid item>
            <TextField
              id="date"
              label="To"
              type="date"
              onChange={e => {
                setEndDate(e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item>
            <Button variant="contained" onClick={buttonAction}> Go </Button>
          </Grid>
        </Grid>

        {showPie ? <PieFromDates sDate={startDate} eDate={endDate} /> : '' }
      </div>
    )
  }

  const PieFromDates = ({sDate, eDate}) => {
    const [user] = useState(AuthService.getCurrentUser());
    const [totalCarbonEmissions, setTotalCarbonEmissions] = useState([]);
    // localhost:5000/api/get/getTotalCarbonOfVehiclesTripsWithDate/14/2021-07-17/2021-07-23
    useEffect(() => {
        let isMounted = true;
        fetch(`/api/get/getTotalCarbonOfVehiclesTripsWithDate/${user.uid}/${sDate}/${eDate}`)
            .then((res) => res.json())
            .then((data) => {
              if (isMounted) {
                setTotalCarbonEmissions(data);
              }
            });
            return () => { isMounted = false};
    }, [user.uid, eDate, sDate]);

    return (
      <div className="pieDate">
        <Chart
          data={totalCarbonEmissions}
          >
            <PieSeries
            valueField="value"
            argumentField="label"
            />
            <Title
            text="Total carbon emission (in grams)"
            />
            <Legend
            orientation="vertical"
            itemTextPosition="right"
            horizontalAlignment="center"
            verticalAlignment="bottom"
            columnCount={4}
            />
            <EventTracker />
            <Tooltip />
        </Chart>
      </div>
    )
  }
