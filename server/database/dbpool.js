import pg from 'pg';
pg.types.setTypeParser(pg.types.builtins.NUMERIC, (val) => {
    if (val === null) {
        return 0;
    } else {
        return parseFloat(val)
    }
})
import Knex from 'knex';
import dotenv from 'dotenv'

dotenv.config()
// const { Pool } = pg;
// export var pool = new Pool({
//     connectionString: process.env.DATABASE_URL
// });


// Connect a connect config to Cloud SQL for PostgreSQL
const createTcpPool = () => {
    return Knex({
        client: 'pg',
        connection: {
            user: process.env.SQL_USER,
            password: process.env.SQL_PASSWORD,
            database: process.env.SQL_DATABASE,
            host: process.env.SQL_HOST,
            port: process.env.SQL_PORT
        },
    });
}

const createPool = () => {
    const config = {pool: {}};
    config.pool.max = 5;
    config.pool.min = 5;
    config.pool.acquireTimeoutMillis = 60000;
    config.createTimeoutMillis = 30000; // 30 seconds
    config.idleTimeoutMillis = 600000; // 10 minutes
    config.createRetryIntervalMillis = 200;

    return createTcpPool(config);
    //return createUnixSocketPool(config);
};

// Set up a variable to hold the connection pool to CloudSQL
export var pool = createPool();