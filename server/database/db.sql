CREATE DATABASE carbonemissions;

CREATE EXTENSION citext;

CREATE TABLE person(
    uid SERIAL PRIMARY KEY,
    username VARCHAR,
    password VARCHAR,
    role VARCHAR,
    country VARCHAR(15),
    gid integer,
);

ALTER TABLE person ADD COLUMN gid integer;
-- ALTER TABLE person ADD FOREIGN KEY (gid) REFERENCES groups(gid) ON DELETE CASCADE;

CREATE TABLE groups (
      gid SERIAL PRIMARY KEY,
      gname VARCHAR,
      uid SERIAL,
      mname VARCHAR,
      created_at DATE,
      membernum integer,
      CONSTRAINT fk_managerid
          FOREIGN KEY(uid) REFERENCES person(uid)
);

CREATE TABLE person_carbon_emissions(
    uid SERIAL,
    totcarbon_vt_g NUMERIC,
    totcarbon_vt_lb NUMERIC,
    totcarbon_vt_kg NUMERIC,
    totcarbon_vt_mt NUMERIC,
    totcarbon_flight_g NUMERIC,
    totcarbon_flight_lb NUMERIC,
    totcarbon_flight_kg NUMERIC,
    totcarbon_flight_mt NUMERIC,
    totcarbon_ord_g NUMERIC,
    totcarbon_ord_lb NUMERIC,
    totcarbon_ord_kg NUMERIC,
    totcarbon_ord_mt NUMERIC,
    CONSTRAINT fk_uid
      FOREIGN KEY(uid) REFERENCES person(uid) ON DELETE CASCADE
);

CREATE TABLE vehicle(
    vid SERIAL PRIMARY KEY,
    uid SERIAL,
    make VARCHAR(50),
    model VARCHAR(50),
    year INTEGER NOT NULL,
    model_id VARCHAR(50),
    CONSTRAINT fk_uid
      FOREIGN KEY(uid) REFERENCES person(uid) ON DELETE CASCADE
);

CREATE TABLE vehicletrips(
    vtid SERIAL PRIMARY KEY,
    vid SERIAL,
    uid SERIAL,
    distance NUMERIC,
    distanceunit VARCHAR(10),
    model_id VARCHAR(50),
    carbon_g NUMERIC,
    carbon_lb NUMERIC,
    carbon_kg NUMERIC,
    carbon_mt NUMERIC,
    date_added DATE NOT NULL,
    CONSTRAINT fk_uid
      FOREIGN KEY(uid) REFERENCES person(uid) ON DELETE CASCADE
);


CREATE TABLE airports (
    iatacode VARCHAR(8) PRIMARY KEY,
    name VARCHAR(250),
    city VARCHAR(150),
    country VARCHAR(150)
);


CREATE TABLE flighttrips(
    fid SERIAL PRIMARY KEY,
    uid SERIAL,
    passengers INTEGER, 
    strt VARCHAR(8),
    dst VARCHAR(8),
    carbon_g NUMERIC,
    carbon_lb NUMERIC,
    carbon_kg NUMERIC,
    carbon_mt NUMERIC,
    date_added DATE NOT NULL,
    CONSTRAINT fk_uid
      FOREIGN KEY(uid) REFERENCES person(uid) ON DELETE CASCADE
);

CREATE TABLE orders(
    oid SERIAL PRIMARY KEY,
    uid SERIAL,
    weight NUMERIC,
    weight_unit VARCHAR(5),
    distance NUMERIC,
    distance_unit VARCHAR(5),
    transmthd VARCHAR(30),
    carbon_g NUMERIC,
    carbon_lb NUMERIC,
    carbon_kg NUMERIC,
    carbon_mt NUMERIC,
    date_added DATE NOT NULL,
    CONSTRAINT fk_uid
      FOREIGN KEY(uid) REFERENCES person(uid) ON DELETE CASCADE
);

CREATE TABLE vehiclemakes(
  id VARCHAR(50),
  make VARCHAR(30) PRIMARY KEY,
  num_models NUMERIC
);

CREATE TABLE vehiclemodels(
  id VARCHAR(50) PRIMARY KEY,
  model VARCHAR(50),
  year INTEGER,
  make VARCHAR(30),
  CONSTRAINT fk_make
      FOREIGN KEY(make) REFERENCES vehiclemakes(make)
);

-- Trigger functions 
CREATE OR REPLACE FUNCTION sum_vt_TotCarbon()
RETURNS TRIGGER 
LANGUAGE PLPGSQL
AS
$$
BEGIN
IF (SELECT COUNT(*) FROM vehicletrips WHERE (uid = NEW.uid) or (uid = OLD.uid) ) > 0 THEN
  UPDATE person_carbon_emissions
  SET totcarbon_vt_g =  carbonSums.gSum, totcarbon_vt_lb = carbonSums.lbSum, totcarbon_vt_kg = carbonSums.kgSum, totcarbon_vt_mt = carbonSums.mtSum
  FROM (
  SELECT SUM(carbon_g) AS gSum, SUM(carbon_lb) AS lbSum, SUM(carbon_kg) AS kgSum, SUM(carbon_mt) AS mtSum, uid
  FROM vehicletrips AS VT
  GROUP BY VT.uid
  ) as carbonSums
  WHERE carbonSums.uid = person_carbon_emissions.uid;
ELSE
  UPDATE person_carbon_emissions 
  SET totcarbon_vt_g =  0, totcarbon_vt_lb = 0, totcarbon_vt_kg = 0, totcarbon_vt_mt = 0
  WHERE person_carbon_emissions.uid = OLD.uid;
END IF;
RETURN NEW;
END;
$$;

CREATE OR REPLACE FUNCTION sum_flighttrips_TotCarbon()
RETURNS TRIGGER 
LANGUAGE PLPGSQL
AS
$$
BEGIN
IF (SELECT COUNT(*) FROM flighttrips WHERE (uid = NEW.uid) or (uid = OLD.uid) ) > 0 THEN
  UPDATE person_carbon_emissions
  SET totcarbon_flight_g = carbonSums.gSum, totcarbon_flight_lb = carbonSums.lbSum, totcarbon_flight_kg = carbonSums.kgSum, totcarbon_flight_mt = carbonSums.mtSum
  FROM (
  SELECT SUM(carbon_g) AS gSum, SUM(carbon_lb) AS lbSum, SUM(carbon_kg) AS kgSum, SUM(carbon_mt) AS mtSum, uid
  FROM flighttrips AS FT
  GROUP BY FT.uid
  ) as carbonSums
  WHERE carbonSums.uid = person_carbon_emissions.uid;
ELSE
  UPDATE person_carbon_emissions 
  SET totcarbon_flight_g = 0, totcarbon_flight_lb = 0, totcarbon_flight_kg = 0, totcarbon_flight_mt = 0
  WHERE person_carbon_emissions.uid = OLD.uid;
END IF;
RETURN NEW;
END;
$$;

CREATE OR REPLACE FUNCTION sum_orders_TotCarbon()
RETURNS TRIGGER 
LANGUAGE PLPGSQL
AS
$$
BEGIN
IF (SELECT COUNT(*) FROM orders WHERE (uid = NEW.uid) or (uid = OLD.uid) ) > 0 THEN
  UPDATE person_carbon_emissions
  SET totcarbon_ord_g = carbonSums.gSum, totcarbon_ord_lb = carbonSums.lbSum, totcarbon_ord_kg = carbonSums.kgSum, totcarbon_ord_mt = carbonSums.mtSum
  FROM (
  SELECT SUM(carbon_g) AS gSum, SUM(carbon_lb) AS lbSum, SUM(carbon_kg) AS kgSum, SUM(carbon_mt) AS mtSum, uid
  FROM orders AS O
  GROUP BY O.uid
  ) as carbonSums
  WHERE carbonSums.uid = person_carbon_emissions.uid;
ELSE
  UPDATE person_carbon_emissions 
  SET totcarbon_ord_g = 0, totcarbon_ord_lb = 0, totcarbon_ord_kg = 0, totcarbon_ord_mt = 0
  WHERE person_carbon_emissions.uid = OLD.uid;
END IF;
RETURN NEW;
END;
$$;

/* Automatically create an row for person in person_carbon_emission table */
CREATE OR REPLACE FUNCTION add_person_carbon_table()
RETURNS trigger 
LANGUAGE PLPGSQL
AS
$$
BEGIN
INSERT INTO person_carbon_emissions (uid, totcarbon_vt_g, totcarbon_vt_lb, totcarbon_vt_kg, totcarbon_vt_mt, totcarbon_flight_g, totcarbon_flight_lb, totcarbon_flight_kg, totcarbon_flight_mt, totcarbon_ord_g, totcarbon_ord_lb, totcarbon_ord_kg, totcarbon_ord_mt) 
VALUES (NEW.uid, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
RETURN NEW;
END;
$$;

-- Triggers 
CREATE TRIGGER create_person_carbon_table_trigger
AFTER INSERT 
ON person
FOR EACH ROW
EXECUTE PROCEDURE add_person_carbon_table();

CREATE TRIGGER vt_carbon_update_trigger
AFTER INSERT OR DELETE OR UPDATE
ON vehicletrips
FOR EACH ROW
EXECUTE PROCEDURE sum_vt_TotCarbon();

CREATE TRIGGER flighttrips_carbon_update_trigger
AFTER INSERT OR DELETE OR UPDATE
ON flighttrips
FOR EACH ROW
EXECUTE PROCEDURE sum_flighttrips_TotCarbon();

CREATE TRIGGER orders_carbon_update_trigger
AFTER INSERT OR DELETE OR UPDATE
ON orders
FOR EACH ROW
EXECUTE PROCEDURE sum_orders_TotCarbon();