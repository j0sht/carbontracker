'use strict';
import express from 'express';
import dotenv from 'dotenv';
import cors from "cors";

import postRoutes from './routes/posts.js';
import getRoutes from './routes/get.js';
import authRoutes from './routes/auth.routes.js'
import userRoutes from './routes/user.routes.js';
import deleteRoutes from "./routes/delete.js";
import updateRoutes from "./routes/update.js";

dotenv.config()

const app = express();

var corsOptions = {
    origin: "http://localhost:3000"
  };

app.enable('trust proxy');
// to parse json requests
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.get("/", (req, res) => {
    res.send("Sever is running.");
});

app.use('/api/auth', authRoutes)
app.use('/api/test', userRoutes)
app.use('/api/get', getRoutes)
app.use('/api/post', postRoutes)
app.use('/api/delete', deleteRoutes)
app.use('/api/update', updateRoutes)


const PORT = process.env.PORT || 3000;
export const server = app.listen(PORT, () => {
    console.log(`App listening at http://localhost:${PORT}`)
});
