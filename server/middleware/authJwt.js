import { secret } from "../config/auth.config.js";
import { pool } from "../database/dbpool.js"

import pkg from 'jsonwebtoken';
const { verify } = pkg;

const verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  verify(token, secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    req.userId = decoded.id;
    next();
  });
};

const isAdmin = async (req, res, next) => {
    await pool('person').where({uid: req.userId}).first().then(user => {
        if (user?.role === "admin") {
            next();
            return;
        }
        res.status(403).send({
            message: "Require Admin Role!"
        });
        return;
    });
};


const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
};
export default authJwt;