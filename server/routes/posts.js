// Add all the post routes here
// logic (call back function) for the post request is in controller/posts.js
import express from 'express';

import {
        addPerson,
        addVehicle,
        addVehicleTrip,
        addFlightTrip,
        addOrders,
        addGroup,
        joinGroup,
        leaveGroup, setUserGroup
} from '../controllers/posts.js';

const routerPost = express.Router();

//TODO add authJwt.js to all calls that should be verified (check ./middleware)

routerPost.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

routerPost.post('/addVehicle', addVehicle);

routerPost.post('/addVehicleTrip', addVehicleTrip);

routerPost.post('/addFlightTrip', addFlightTrip);

routerPost.post('/addOrders', addOrders);

routerPost.post('/addGroup', addGroup);

routerPost.post('/joinGroup/:uid/:gid', joinGroup);

routerPost.post('/leaveGroup/:uid/:gid', leaveGroup);

routerPost.post('/updateGroupId/:uid/:gid', setUserGroup)

export default routerPost;