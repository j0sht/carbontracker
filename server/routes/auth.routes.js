import { signup, signin } from "../controllers/auth.controller.js";

import express from 'express';
import verifySignup from "../middleware/verifySignUp.js";

 // CHECK

const routerAuth = express.Router();


routerAuth.use(function(req, res, next) {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});

routerAuth.post(
    "/signup",
    [verifySignup.checkDuplicateUsername],
    signup
);

routerAuth.post("/signin", signin);

export default routerAuth;