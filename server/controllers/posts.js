import {pool} from '../database/dbpool.js';
import axios from 'axios';

/*
    Logic for inserting person
    note 1: uid is not required because it is serial - a uid will be assigned when person is added
    note 2: totcarbonem should not be required - we update this based on information inputted (such as vehicle trips, orders, etc)
    note 3: Can't add duplicate emails, each email has to be unique
    req.body: name, email, country
*/
const insertPerson = async (pool, data) => {
    try {
      return await pool('person').insert(data).returning('uid');
    } catch (err) {
      throw Error(err);
    }
  };

export const addPerson = async (req, res) => {
    try {
        var uid = await insertPerson(pool, req.body);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to cast person; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"uid": ${uid}}`).end();
}

/*
    uid is fk, so if uid does not exist in person table, then this will fail
    vid is not required
    req.body: uid, make, model, year
*/
const insertVehicle = async (pool, data) => {
    try {
      return await pool('vehicle').insert(data).returning('vid');
    } catch (err) {
      throw Error(err);
    }
  };

export const addVehicle = async (req, res) => {
    try {
        var vid = await insertVehicle(pool, req.body);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to cast vehicle; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"vid": ${vid}}`).end();
}

/*
    uid & vid are fk, this info is required
    vtid is not requierd - it is serial
    req.body: vid, uid, distance, distanceunit, model_id
*/
const insertTrip = async (pool, data) => {
    try {
      return await pool('vehicletrips').insert(data);
    } catch (err) {
      throw Error(err);
    }
  };

export const addVehicleTrip = async (req, res) => {
    /*
    Be judicous with how you test this, only have 200 api calls per month...
    NOTE: I am unable to return vtid after making axios call.. if someone can help that would be great
    */
    axios
    .post(process.env.CARBON_INTERFACE_URL, {
        type: "vehicle", //fixed value
        "distance_unit": req.body.distanceunit,
        "distance_value": req.body.distance,
        "vehicle_model_id": req.body.model_id
    }, {
        headers: {
            "Authorization": process.env.__CARBON_INTERFACE_API_KEY_PK
        }
    })
    .then(carbonRes => {
        const carbonData = carbonRes.data;
        const data = {
            vid: req.body.vid,
            uid: req.body.uid,
            distance: req.body.distance,
            distanceunit: req.body.distanceunit,
            model_id: req.body.model_id,
            carbon_g: carbonData.data.attributes.carbon_g,
            carbon_lb: carbonData.data.attributes.carbon_lb,
            carbon_kg: carbonData.data.attributes.carbon_kg,
            carbon_mt: carbonData.data.attributes.carbon_mt,
            date_added: req.body.date_added
        };
        // console.log(data)
        try {
            insertTrip(pool, data)
            .then(_ => res.status(200).send('Added trip').end());
        } catch (err) {
            console.log(err)
            return res.status(500)
                .send('Unable to cast vehicle trip; see logs for more details.')
                .end();
        }
    })
}

/*
    uid is fk, so if uid does not exist in person table, then this will fail
    fid is not required
    req.body: passengers: int, strt: varchar, dst: varchar, carbonemissions
    IMPORTANT NOTE: req.body might change based on info required for carbon interface api
*/
const insertFlightTrips = async (pool, data) => {
    try {
      return await pool('flighttrips').insert(data);
    } catch (err) {
      throw Error(err);
    }
  };

export const addFlightTrip = async (req, res) => {
    /* Insert logic to calculate carbon emission for flight trip */
    axios
        .post(process.env.CARBON_INTERFACE_URL, {
            "type": "flight",
            "passengers": req.body.passengers,
            "legs": [
                {"departure_airport": req.body.strt, "destination_airport": req.body.dst},
                {"departure_airport": req.body.dst, "destination_airport": req.body.strt}
            ]
        }, {
            headers: {
                "Authorization": process.env.__CARBON_INTERFACE_API_KEY_PK
            }
        })
        .then(carbonRes => {
            const carbonData = carbonRes.data;
            const data = {
                uid: req.body.uid,
                passengers: req.body.passengers,
                strt: req.body.strt,
                dst: req.body.dst,
                // date: carbonData.data.attributes.estimated_at,
                carbon_g: carbonData.data.attributes.carbon_g,
                carbon_lb: carbonData.data.attributes.carbon_lb,
                carbon_kg: carbonData.data.attributes.carbon_kg,
                carbon_mt: carbonData.data.attributes.carbon_mt,
                date_added: req.body.date_added
            };
        try {
            insertFlightTrips(pool, data)
             .then(_ => res.status(200).send("Added trip").end());
        } catch (err) {
            console.log(err)
            return res.status(500)
                .send('Unable to cast flighttrips; see logs for more details.')
                .end();
        }})
}

/*
    uid is fk
    oid is not required
    req.body: weight: numeric, weight_unit: varchar, distance: numeric, distance_unit: varchar, transmthd: varchar, carbonemissions
    req body might change
*/
const insertOrders = async (pool, data) => {
    try {
      return await pool('orders').insert(data);
    } catch (err) {
      throw Error(err);
    }
  };

export const addOrders = async (req, res) => {
    /* Insert logic to calculate carbon emission for orders */
    axios.post(process.env.CARBON_INTERFACE_URL, {
            type: "shipping", //fixed value
            "weight_value": req.body.weight,
            "weight_unit": req.body.weight_unit,
            "distance_value": req.body.distance,
            "distance_unit": req.body.distance_unit,
            "transport_method": req.body.transmthd,
        }, {
            headers: {
                "Authorization": process.env.__CARBON_INTERFACE_API_KEY_PK
            }
        })
        .then(carbonRes => {
            const shippingCarbon = carbonRes.data;
            const data = {
                uid: req.body.uid,
                weight: req.body.weight,
                weight_unit: req.body.weight_unit,
                distance: req.body.distance,
                distance_unit: req.body.distance_unit,
                transmthd: req.body.transmthd,
                // date: shippingCarbon.data.attributes.estimated_at,
                carbon_g: shippingCarbon.data.attributes.carbon_g,
                carbon_lb: shippingCarbon.data.attributes.carbon_lb,
                carbon_kg: shippingCarbon.data.attributes.carbon_kg,
                carbon_mt: shippingCarbon.data.attributes.carbon_mt,
                date_added: req.body.date_added
            };
            try {
                insertOrders(pool, data)
                  .then(_ => res.status(200).send("Added order").end());
            } catch (err) {
                console.log(err)
                return res.status(500)
                    .send('Unable to cast orders; see logs for more details.')
                    .end();
            }
        })
}

const isAGroupMember = async (pool, uid) => {
    return await pool
                .raw(`SELECT gid IS NOT NULL AS ismember
                        FROM person
                        WHERE uid=?;`, [uid]);
};

const insertGroup = async (pool, data) => {
    try {
      const isMember = await isAGroupMember(pool, data.uid);
      console.log(isMember);
      if(isMember.rows[0].ismember == false)
      {
        const gid =  await pool('groups').insert(data).returning('gid');
        await pool('person').update({"gid": gid[0]}).where('uid', data.uid);
        return gid;
      }
      throw 'A person cannot create a new group if they already belong to one.'
    } catch (err) {
      throw Error(err);
    }
  };

export const addGroup = async (req, res) => {
    try {
        const data = {
            gname: req.body.gname,
            mname: req.body.mname,
            uid:   req.body.uid,
            membernum: 1,
            created_at: (new Date()).toISOString().slice(0, 10)
        }
        var managerGid = await insertGroup(pool, data);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to cast group; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"gid": ${managerGid}}`).end();
}

const joinGroupQuery = async (pool, uid, gid) => {
    try {
      const isMember = await isAGroupMember(pool, uid);
      if(isMember.rows[0].ismember == false)
      {
          const groupID =  await pool('person').update({"gid": gid}).where('uid', uid).returning('gid');
          await pool
            .raw(`UPDATE groups
                    SET membernum = membernum + 1
                    WHERE gid=?;`, [gid]);
          return groupID;
      }
      else
      {
          throw 'A person cannot join a new group if they are already part of a group.';
      }
    } catch (err) {
      throw Error(err);
    }
  };

export const joinGroup = async (req, res) => {
    try {
        var gid = await joinGroupQuery(pool, req.params.uid, req.params.gid);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to cast group; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"gid": ${gid}}`).end();
};

const setUserGroupQuery = async (pool, uid, gid) => {
    try {

        await pool
                .raw(`UPDATE person
                    SET gid =?
                    WHERE uid =?;`, [gid], [uid]);
    } catch (err) {
        throw Error(err);
    }
};

export const setUserGroup = async (req, res) => {
    try {
        await setUserGroupQuery(pool, req.params.uid, req.params.gid);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to set user group id; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"gid": ${gid}}`).end();
};

const deleteGroupQuery = async (pool, gid) => {
    try {
        await pool.delete().from('groups').where('gid', gid);
        await pool('person').update({'gid' : null}).where('gid', gid); 
    } catch (err) {
        throw Error(err);
    }
};

const leaveGroupQuery = async (pool, uid, gid) => {
    try {
      const isManager =  await pool
                                .raw(`SELECT uid=? AS ismanager
                                FROM groups
                                WHERE gid=?;`, [uid, gid]);
      const belongsToGroup = await pool
                                    .raw(`SELECT
                                        CASE
                                            WHEN EXISTS( SELECT 1 FROM person WHERE uid=? AND gid=? )
                                                 THEN true 
                                            ELSE false 
                                        END`, [uid, gid]);
      if(isManager.rows[0].ismanager == false && belongsToGroup.rows[0].case == true)
      {
        await pool('person').update({'gid': null}).where('uid', uid);
        await pool
            .raw(`UPDATE groups
                    SET membernum = membernum - 1
                    WHERE gid=?;`, [gid]);
      }
      else if(isManager.rows[0].ismanager == true)
      {
        await deleteGroupQuery(pool, gid);
      }
      else
      {
          throw 'Person must belong to the group'
      }
      return isManager;
    } catch (err) {
      throw Error(err);
    }
  };

export const leaveGroup = async (req, res) => {
    try {
        var isManager = await leaveGroupQuery(pool, req.params.uid, req.params.gid);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to cast group; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"ismanager": ${isManager.rows[0].ismanager}}`).end();
};
