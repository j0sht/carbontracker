import { secret } from "../config/auth.config.js";
import { pool } from "../database/dbpool.js";

import pkgJwt from "jsonwebtoken";
import pkgBcrypt from "bcryptjs";
const { sign } = pkgJwt;
const { hashSync, compareSync } = pkgBcrypt;

export async function signup(req, res) {
  // Save person to Database
  const hashedPwd = hashSync(req.body.password, 8)
  await pool('person').insert({
    username: req.body.username,
    password: hashedPwd,
    role: req.body.role || 'user'
  })
    .then(() => {
        res.send({ message: "User was registered successfully! You can now log in to access the app." });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
}

export async function signin(req, res) {
    await pool('person').where({
        username: req.body.username
    }).first()
    .then(user => {
        if (!user) {
            return res.status(404).send({ message: "Incorrect username or password!" });
        }

        var passwordIsValid = compareSync(
            req.body.password,
            user.password
        );

        if (!passwordIsValid) {
            return res.status(401).send({
            accessToken: null,
            message: "Incorrect username or password!"
            });
        }

        var token = sign({ id: user.uid }, secret, {
            expiresIn: 86400 // 24 hours
        });
        res.status(200).send({
            uid: user.uid,
            username: user.username,
            role: user.role,
            accessToken: token
        });
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    });
}