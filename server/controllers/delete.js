import {pool} from '../database/dbpool.js';

const deleteFlightQuery = async (pool, id) => {
    try {
        return await pool.delete().from('flighttrips').where('fid', id);
    } catch (err) {
        throw Error(err);
    }
};

export const deleteFlight = async (req, res) => {
    try {
        const fid = req.params.fid;
        await deleteFlightQuery(pool, fid);
        console.log(`flight #${fid} was deleted`);
        return res.status(200).send(`flight #${fid} was deleted`).end();
    } catch(err){
        console.error(err);
        return res.status(500)
            .send('Unable to delete the flight; see logs for more details.')
            .end();
    }
}

const deleteVehicleQuery = async (pool, id) => {
    try {
        return await pool.delete().from('vehicle').where('vid', id);
    } catch (err) {
        throw Error(err);
    }
};

export const deleteVehicle = async (req, res) => {
    try {
        const id = req.params.vid;
        await deleteVehicleQuery(pool, id);
        console.log(`vehicle #${id} was deleted`);
        return res.status(200).send(`vehicle #${id} was deleted`).end();
    } catch(err){
        console.error(err);
        return res.status(500)
            .send('Unable to delete the vehicle; see logs for more details.')
            .end();
    }
}


const deleteVehicleTripQuery = async (pool, id) => {
    try {
        return await pool.delete().from('vehicletrips').where('vtid', id);
    } catch (err) {
        throw Error(err);
    }
};

export const deleteVehicleTrip = async (req, res) => {
    try {
        const id = req.params.vtid;
        await deleteVehicleTripQuery(pool, id);
        console.log(`vehicle trip #${id} was deleted`);
        return res.status(200).send(`vehicle trip #${id} was deleted`).end();
    } catch(err){
        console.error(err);
        return res.status(500)
            .send('Unable to delete the vehicle trip; see logs for more details.')
            .end();
    }
}

const deleteOrderQuery = async (pool, id) => {
    try {
        return await pool.delete().from('orders').where('oid', id);
    } catch (err) {
        throw Error(err);
    }
};

export const deleteOrder = async (req, res) => {
    try {
        const id = req.params.oid;
        await deleteOrderQuery(pool, id);
        console.log(`Order #${id} was deleted`);
        return res.status(200).send(`Order #${id} was deleted`).end();
    } catch(err){
        console.error(err);
        return res.status(500)
            .send('Unable to delete the order; see logs for more details.')
            .end();
    }
}

const isPersonManager = async (pool, uid, gid) => {
    return await pool
                .raw(`SELECT uid=? AS ismanager
                    FROM groups
                    WHERE gid=?;`, [uid, gid]);
    
}

const isMember = async (pool, uid) => {
    return await pool
        .raw(`SELECT gid
            FROM person
            WHERE uid=?;`, [uid]);
}

const removePerson = async (pool, id) => {
    return pool.delete().from('person').where('uid', id);
}

const deletePersonQuery = async (pool, id) => {
    try {
        const getGid = isMember(pool, id);
        getGid.then(res => {
            if(res.rows[0].gid == null)
            {
                return removePerson(pool, id);
            } else {
                var personGid = res.rows[0].gid;
                const isManager = isPersonManager(pool, id, personGid);
                isManager.then(result => {
                    if(result.rows[0].ismanager == true)
                    {
                        const delGid = pool.delete().from('groups').where('gid', personGid).returning('gid');
                        const updateGid = pool('person').update({'gid' : null}).where('gid', personGid);
                        updateGid.then(updateRes => {
                            console.log(updateRes);
                        })
                        delGid.then( delRes => {
                            return removePerson(pool, id);
                        });

                    } else {
                        const updateGid = pool.raw(`UPDATE groups
                                SET membernum = membernum - 1
                                WHERE gid=?;`, [personGid]);
                        updateGid.then( updateRes => {
                        return removePerson(pool, id);
                        });
                    }
                });
            }
        })
    } catch (err) {
        throw Error(err);
    }
};

export const deletePerson = async (req, res) => {
    try {
        const id = req.params.uid;
        await deletePersonQuery(pool, id);
        return res.status(200).send(`Person with uid:${id } was deleted.`).end();
    } catch(err) {
        console.log(err);
        return res.status(500)
        .send('Unable to delete the person; see logs for more details.')
        .end();
    }
}

const deleteGroupQuery = async (pool, gid) => {
    try {
        await pool.delete().from('groups').where('gid', gid);
        await pool('person').update({'gid' : null}).where('gid', gid); 
    } catch (err) {
        throw Error(err);
    }
};

export const deleteGroup = async (req, res) => {
    try {
        const uid = req.params.uid;
        const gid = req.params.gid;
        const isManager =  await pool
                                .raw(`SELECT uid=? AS ismanager
                                FROM groups
                                WHERE gid=?;`, [uid, gid]);
        if(isManager.rows[0].ismanager == false)
        {
            throw 'Only manager can delete group';
        }
        await deleteGroupQuery(pool, gid);
        return res.status(200).send(`Group with gid:${gid } was deleted.`).end();
    } catch(err) {
        console.log(err);
        return res.status(500)
        .send('Unable to delete the group; see logs for more details.')
        .end();
    }
}
